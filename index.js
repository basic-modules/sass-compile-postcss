const fs = require('fs')
const path = require('path')
const shell = require('shelljs')
const sass = require('node-sass')
const postcss = require('postcss')

function compileSass(source, target, callback) {
  console.log('Start compile sass to css...')

  if (!fs.existsSync(source))
    throw new Error(`File doesn't exists '${source}'.`)

  // targetDir 없을 경우 새로 생성
  let targetDir = path.dirname(target)
  if (!fs.existsSync(targetDir)) {
    console.log(`Make directionry recursively '${targetDir}'`)
    shell.mkdir('-p', targetDir)
  }

  console.log(`${source} \n-> ${target}`)

  sass.render(
    {
      file: source
    },
    callback
  )
}

function compile (source, target, options={}, finishCallback) {
  options = Object.assign({
    /**
     * false 일 경우, sass 컴파일에서 끝난다.
     */
    postcss: true,
    postcssModules: true,
    postcssModulesOptions: {},
    /**
     * true일 경우, cssnano 기본 설정을 사용한다.
     */
    cssnano: true,
    cssnanoOptions: {},
    plugins: [],
  }, options)

  compileSass(source, target, function(err, result) {
    if (err) {
      console.log(`Error occured while compile '${source}' file.`)
      throw err
    }
    try {
      let css = result.css.toString()

      // sass 컴파일 후 종료
      if (!options.postcss) {
        fs.writeFileSync(target, css)
        if (typeof finishCallback === 'function') finishCallback(target, css)
        return
      }

      let postcssPlugins = [];

      /**
       * postcss-modules 옵션
       */
      if (options.postcssModules) {
        let postcssModulesOptions = Object.assign({
        }, options.postcssModulesOptions)
        postcssPlugins.push(require('postcss-modules')(postcssModulesOptions))
      }

      /**
       * nanocss 옵션
       */
      if (options.cssnano) {
        postcssPlugins.push(require('cssnano')(Object.assign({
          preset: ['default', { discardEmpty: false }],
        }, options.cssnanoOptions)))
      }

      /**
       * postcss 컴파일
       */
      console.log(`Start compiling to postcss...\n->${target}`)
      postcss(postcssPlugins.concat(options.plugins)) // postcss 모듈 추가
        .process(css, {from: undefined})
        .then(postcssResult => {
          // postcss 결과 저장
          fs.writeFileSync(target, postcssResult.css)
          if (typeof finishCallback === 'function') finishCallback(target, postcssResult.css)
        })
        .catch(err => {
          console.log(`Error occured postcss.. '${target}' file.`)
          console.log(err)
        })
    } catch (e) {
      console.log(`Error occured when save '${target}' file.`)
      throw e
    }
  })
}

module.exports = {
  compile: compile,
  compoileSass: compileSass
}
