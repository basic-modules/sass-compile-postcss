# sass-compile-postcss

> sass->postcss 를 위한 모듈이다.

## 설명
크게 아래와 같은 시퀀스를 따른다.

scss/sass 파일 -> css 변환 -> postcss 적용

현재 기본 플러그인으로 `postcssModules`, `cssnano`를 사용한다.

## How to use
```js
const sassCompilePostcss = require('./sass-compile-postcss')
sassCompilePostcss.compile('./a.scss', './b.css', {
plugins: [autoprefixer]
})
```
